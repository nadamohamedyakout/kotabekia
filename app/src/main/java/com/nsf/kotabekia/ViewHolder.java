package com.nsf.kotabekia;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;


public class ViewHolder extends RecyclerView.ViewHolder {

    ImageView imageCategory1;
    ImageView imageFavorite1;


    public ViewHolder(View itemView) {
        super(itemView);

        imageCategory1 = (ImageView) itemView.findViewById(R.id.image_catogery1);
        imageFavorite1 = (ImageView) itemView.findViewById(R.id.favorite1);
    }
}

