package com.nsf.kotabekia;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class RecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {
    ArrayList<Category> categories;
    Context context;
    LayoutInflater inflater;

    public RecyclerViewAdapter(ArrayList<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recycler_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Category category = categories.get(position);
        Glide.with(context).load(category.getImage1()).into(holder.imageCategory1);
        if (category.isLiked()) {
            holder.imageFavorite1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // make unlike
                }
            });
        } else {
            holder.imageFavorite1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // make like
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}

