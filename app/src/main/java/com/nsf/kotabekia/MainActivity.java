package com.nsf.kotabekia;

import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static com.nsf.kotabekia.R.id.drawer;
import static com.nsf.kotabekia.R.menu.drawermenu;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;

    DrawerLayout drawerLayout;

    NavigationView navigationView;

    ActionBarDrawerToggle toggle;

    RecyclerView recyclerView;

    Button button;

    Intent intentlogin;
    Intent intentprofile;
    Intent intenteditprofile;
    Intent intenthelpcenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(drawer);
        navigationView = (NavigationView) findViewById(R.id.navigation);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        button = (Button) findViewById(R.id.login_button);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.profile:
                        intentprofile = new Intent(MainActivity.this, LoginPage.class);
                        startActivity(intentprofile);
                        break;
                    case R.id.editprofile:
                        intenteditprofile = new Intent(MainActivity.this, LoginPage.class);
                        startActivity(intenteditprofile);
                        break;
                    case R.id.helpcenter:
                        intenthelpcenter = new Intent(MainActivity.this, LoginPage.class);
                        startActivity(intenthelpcenter);
                        break;
                }
                if (item.isChecked()) {
                    item.setChecked(false);
                } else {
                    item.setChecked(true);
                }
                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        button.setPaintFlags(button.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        ArrayList<Category> categories = new ArrayList<>();
        categories.add(new Category("-skjdklsdj", "https://www.w3schools.com/html/pulpitrock.jpg", false));
        categories.add(new Category("-skjdklsrrj", "https://www.w3schools.com/html/pulpitrock.jpg", false));
        categories.add(new Category("-skjdklsdjff", "https://www.w3schools.com/html/pulpitrock.jpg", false));
        categories.add(new Category("-skjdklsdjdsa", "https://www.w3schools.com/html/pulpitrock.jpg", false));
        categories.add(new Category("-skjdklsdvvc", "https://www.w3schools.com/html/pulpitrock.jpg", false));

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(categories, this);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);


    }

    public void btnlogin(View V) {
        intentlogin = new Intent(getApplicationContext(), LoginPage.class);
        startActivity(intentlogin);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return true;

    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
            return;
        }

    }

}

