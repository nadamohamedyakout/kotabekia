package com.nsf.kotabekia;


public class Category {
    private String id;
    private String image1;
    private boolean isLiked;

    public Category(String id, String image1, boolean isLiked) {
        this.image1 = image1;
        this.isLiked = isLiked;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }
}
